## Initial machine configuration

Assumes a freshly created machine instance (like Centos 6.5) and access
to some form of a root console.

----------------------------------------------------------------------------------------------------------

#### Disable SELinux configuration

    # login: root

    echo 'SELINUX=disabled' > /etc/sysconfig/selinux

#### Leg up

    # login: root

    rpm -Uvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
    yum install moreutils -y

#### Reboot

    reboot

#### Harden SSH

    # login: root

    sshd="/etc/ssh/sshd_config"
    < ${sshd} \
    egrep -v \
    "^PermitRootLogin|^PasswordAuthentication|^ChallengeResponseAuthentication|^TCPKeepAlive|^ClientAliveInterval|^Port" \
    | sponge ${sshd}
    sed -i '$a \\'                                 ${sshd}
    sed -i '$a PermitRootLogin without-password'   ${sshd}
    sed -i '$a PasswordAuthentication no'          ${sshd}
    sed -i '$a ChallengeResponseAuthentication no' ${sshd}
    sed -i '$a TCPKeepAlive yes'                   ${sshd}
    sed -i '$a ClientAliveInterval 300'            ${sshd}
    sed -i '$a Port 750'                           ${sshd}

#### SSH set up

    # login: root

    mkdir ${HOME}/.ssh
    touch ${HOME}/.ssh/authorized_keys
    chmod 755 ${HOME}/.ssh
    chmod 644 ${HOME}/.ssh/authorized_keys

    [ copy: conductor key to ~root/.ssh/authorized_keys ]

#### Variable for the planned fully qualified domain name

    fqdn="____machine__name____" # ie. spew.7-5-0.com

#### Set machine HOSTNAME

    # login: root

    sed -i '$i hostname '${fqdn} /etc/rc.local
    sed -i '$i \\' /etc/rc.local
    sed -i 's/HOSTNAME=.*$/HOSTNAME='${fqdn}'/' /etc/sysconfig/network

#### Reboot

    reboot

#### Install rsync

    # login: root

    yum install rsync -y

#### Install jinja which does the python templating

    # login: root

    curl -O http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
    rpm -Uvh remi-release-6*.rpm epel-release-6*.rpm

    yum install python-pip -y
    pip install jinja2

#### Reset root password

    # login: root

    passwd    # if root password needs to be changed.

#### Reboot

    reboot
