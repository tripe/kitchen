## Flavour the machine

Assumes an [initialised](../initial/) machine.

----------------------------------------------------------------------------------------------------------

## Flavouring is performed from a conductor.

#### Specify _machine_, _flavour_ and _distro_

    # login: baton@conductor

    machine={____machine__name____}  # make sure to set this properly

    flavour={dns,worker,vanilla}
    distro={centos,ubuntu}
    short=`echo ${machine} | sed 's_\..*$__'`

    echo ${short}


#### Add new machine to ssh config

    # login: baton@conductor

    >> ${HOME}/.ssh/config < EOF
    Host ${short}
        Hostname ${machine}
        User root
        Port 750
    EOF

    cd ${HOME}/work/tripe/pan
    git commit -am "add ${short}" && git push origin

#### Up to date conductor

    # login: baton@conductor

    git_up

#### Transfer and run _adder_

    # login: baton@conductor

    rsync -aH ${HOME}/work/tripe/adder ${short}:

    ssh ${short} adder/${flavour}.${distro}
