## Conductor configuration

Assumes a freshly created machine instance (like Centos 6.5) and access to some
form of a root console.

----------------------------------------------------------------------------------------------------------
Right, just before we get started there is a wee bit of parameterisation need here.  This
is to do with the _hood_ you are setting up.  The _hood_ comprises a logical working
group of machines.

Recommend setting these values below in some local file...

    conductor__name="freddie"
    hood__name="lum"
    hood__dns="4-2.com"
    hood__ssh_port="420"
    hood__git_dns_url="git@queen:r/hood/lum/dns"
    hood__git_things_url="git@queen:r/hood/lum/things"

    country=Australia
    locality=Brisbane

----------------------------------------------------------------------------------------------------------

#### Name setup

    [ -z ${conductor__name} ] && echo -e '\n\nset ${conductor__name}\n\n^C' && sleep 1000
    [ -z ${hood__dns} ]       && echo -e '\n\nset ${hood__dns}      \n\n^C' && sleep 1000

    conductor__fqdn=${conductor__name}.${hood__dns}

#### Disable SELinux configuration

    # login: root

    echo 'SELINUX=disabled' > /etc/sysconfig/selinux

#### Leg up

    # login: root

    rpm -Uvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
    yum install moreutils -y

#### Reboot

    reboot

#### Create a `bootstrap` user

Note the `bootstrap` user will be deleted later.

    # login: root

    useradd bootstrap

#### SSH set up

    # login: root

    mkdir ~bootstrap/.ssh
    touch ~bootstrap/.ssh/authorized_keys
    chmod 755 ~bootstrap/.ssh
    chmod 644 ~bootstrap/.ssh/authorized_keys
    chown -R bootstrap ~bootstrap/.ssh

    [ copy: build key to ~bootstrap/.ssh/authorized_keys ]
    [ assert: ssh access to bootstrap@{conductor__fqdn} using key ]

#### Harden SSH

    # login: bootstrap -> root

    [ -z ${hood__ssh_port} ] && echo -e '\n\nset ${hood__ssh_port}\n\n^C' && sleep 1000

    sshd="/etc/ssh/sshd_config"
    < ${sshd} \
    egrep -v \
    "^PermitRootLogin|^PasswordAuthentication|^ChallengeResponseAuthentication|^TCPKeepAlive|^ClientAliveInterval|^Port" \
    | sponge ${sshd}
    sed -i '$a \\'                                 ${sshd}
    sed -i '$a PermitRootLogin no'                 ${sshd}
    sed -i '$a PasswordAuthentication no'          ${sshd}
    sed -i '$a ChallengeResponseAuthentication no' ${sshd}
    sed -i '$a TCPKeepAlive yes'                   ${sshd}
    sed -i '$a ClientAliveInterval 300'            ${sshd}
    sed -i '$a Port '${hood__ssh_port}             ${sshd}

#### Set machine HOSTNAME

    # login: bootstrap -> root

    [ -z ${conductor__fqdn} ] && echo -e '\n\nset ${conductor__fqdn}\n\n^C' && sleep 1000

    sed -i '$i hostname '${conductor__fqdn} /etc/rc.local
    sed -i '$i \\' /etc/rc.local
    sed -i 's/HOSTNAME=.*$/HOSTNAME='${conductor__fqdn}'/' /etc/sysconfig/network

#### Set timezone

    # set country / locality

    localtime="/usr/share/zoneinfo/${country}/${locality}"
    [ -z ${localtime} ] && echo -e '\n\nno such localtime ${localtime}\n\n^C' && sleep 1000
     
    rm -f /etc/localtime
    ln -s ${localtime} /etc/localtime

#### Reboot

    reboot

#### Install minimal package list

    # login: bootstrap -> root

    yum -y install vim
    yum -y install git
    yum -y install openssh-server
    yum -y install openssh-clients
    yum -y install rsync
    yum -y install telnet
    yum -y install bind-utils
    yum -y install bc
    yum -y install man

#### Configure root tools

    # login: bootstrap -> root

    git clone https://bitbucket.org/tripe/kitchen.git /root/work/tripe/kitchen

#### Set up links

    # login: bootstrap -> root

    ln -s /root/work/tripe/kitchen/conductor/users/root/bin /root/bin

#### Set up cron SSH key sweep ... every minute

    # login: bootstrap -> root

    cat >> /etc/cron.d/ssh_sweep << EOF
    PATH=${PATH}:/root/bin:/bin:/usr/bin
    * * * * * root ssh_sweep
    EOF

#### Reboot

    reboot

----------------------------------------------------------------------------------------------------------

#### Note on the `podium` and `baton` users...

Access to the conductor is achieved first by having a current private key
matching a public key associated with the user `podium` and then knowing
the password for the user `baton`.

The `podium` user exists only as the SSH key pair entry point.  There is
a cron job (ssh_sweep) which wipes out any `.ssh/authorized_keys` entries
which does not match the security model.  All
public keys must be prepended with the following:

    command="echo 'Grab the baton...'; TERM=${TERM}/follow su - baton",no-X11-forwarding,no-port-forwarding

Correspondingly the `baton` user cannot be accessed via SSH.  The only
pathway is via the `podium` user.

Updating the `~podium/.ssh/authorized_keys` generally involves performing a `git pull` on
the keys work area within the `podium` account, although if you are really paranoid
you will likely just maintain the keys locally.

If you are managing the `podium` keys locally then skip the symbolic link step below
and manually create the `~podium/.ssh/hood__authorized_keys` file.

Once all accounts are set up the way to access the `podium` account is to login
as `baton`, `su - root` then `su - podium`.
----------------------------------------------------------------------------------------------------------

## Create `podium` user

    # login: bootstrap -> root

    useradd -u 6999 -g wheel podium

#### Generate SSH key for reading key repos (if this is your approach)

    # login: bootstrap -> root -> podium

    su - podium  # remember

    mkdir ~podium/.ssh
    chmod 755 ~podium/.ssh
    chown -R podium ~podium/.ssh

    ssh-keygen -q -P '' -t rsa -b 4096 -f ~podium/.ssh/id_rsa

    cat ~podium/.ssh/id_rsa.pub

#### Add key to things repository (generally this will be a _quite_ secure standalone repository)

    cat ~podium/.ssh/id_rsa.pub

    # -> repo ['things']

#### Checkout keys for access to `podium` user

    # login: bootstrap -> root -> podium

    [ -z ${hood__name}           ] && echo -e '\n\nset ${hood__name}          \n\n^C' && sleep 1000
    [ -z ${hood__git_things_url} ] && echo -e '\n\nset ${hood__git_things_url}\n\n^C' && sleep 1000

    cd ~podium
    git clone ${hood__git_things_url} work/hood/${hood__name}/things

#### Complete conductor access via repo keys

    # login: bootstrap -> root -> podium
    
    [ -z ${hood__name} ] && echo -e '\n\nset ${hood__name}\n\n^C' && sleep 1000

    ln -s ${HOME}/work/hood/${hood__name}/things/conductor/podium/ssh/authorized_keys ~podium/.ssh/hood__authorized_keys

    # Note the ssh_sweep command will write this file across
    # to ~podium/.ssh/authorized_keys every minute as long
    # as the entries meet the requirement for the 'su - baton' command.

## Create `baton` user

    # login: bootstrap -> root

    useradd -u 7001 -g wheel baton

#### SSH set up 

    # login: bootstrap -> root -> baton

    su - baton  # remember

    mkdir ~baton/.ssh
    touch ~baton/.ssh/authorized_keys
    chmod 755 ~baton/.ssh
    chmod 644 ~baton/.ssh/authorized_keys
    chown -R baton ~baton/.ssh

#### Create some initial directories

    # login: bootstrap -> root -> baton

    cd ~baton
    mkdir bin dl tmp

#### Set password for `baton` user

    # login: bootstrap -> root

    passwd baton

    # note: This password will become a shared token for those accessing the
    #       conductor.

#### Reboot

    reboot

#### Check `podium` to `baton` access

    # assert: login to `baton` via `podium` using external SSH works

#### Delete `bootstrap` user

    # login: bootstrap -> root

    userdel -r bootstrap

#### Create script capture area for `baton` login sessions

    # login: bootstrap -> root

    mkdir /var/log/follow
    chown baton /var/log/follow

#### Generate `PASSPHRASE PROTECTED` ssh key for baton user

    # login: podium -> baton

    passphrase=`uuidgen`
    ssh-keygen -N ${passphrase} -f ${HOME}/.ssh/id_rsa -t rsa -b 4096
    echo; echo; echo; echo "SAVE IMMEDIATELY..."; echo; echo ${passphrase}; echo
    unset passphrase

#### Install ssh key on hood `dns` and `things` repositories

    cat ~podium/.ssh/id_rsa.pub

    # -> repo ['dns','things']

#### Install base repositories

    # login: podium -> baton

    cd ${HOME}

    git clone https://github.com/maddogdavis/strap.git        work/strap
    git clone https://github.com/maddogdavis/dns-frondle.git  work/dns-frondle

    git clone https://bitbucket.org/tripe/kitchen.git         work/tripe/kitchen
    git clone https://bitbucket.org/tripe/adder.git           work/tripe/adder
    git clone https://bitbucket.org/tripe/offal.git           work/tripe/offal
    git clone https://bitbucket.org/tripe/conduct.git         work/tripe/conduct

#### Wire in profile extension which includes pathing and ssh agent

    # login: podium -> baton

    cd ${HOME}/bin
    ln -s ${HOME}/work/tripe/kitchen/conductor/users/baton/bin kitchen

    >> .bash_profile echo 'source ${HOME}/bin/kitchen/bash_extension' 

#### Reboot

    reboot

#### Add SSH key to agent

    # login: podium -> baton

    ssh-add

    # note: The conductor private key remains unlocked for the uptime
    #       duration.  Perhaps this is worthy of consideration.

#### Install other repositories

    # login: podium -> baton

    [ -z ${hood__name}           ] && echo -e '\n\nset ${hood__name}          \n\n^C' && sleep 1000
    [ -z ${hood__git_dns_url}    ] && echo -e '\n\nset ${hood__git_dns_url}   \n\n^C' && sleep 1000
    [ -z ${hood__git_things_url} ] && echo -e '\n\nset ${hood__git_things_url}\n\n^C' && sleep 1000

    git clone ${hood__git_dns_url}    work/hood/${hood__name}/dns
    git clone ${hood__git_things_url} work/hood/${hood__name}/things

#### Set git details

    # login: podium -> baton

    [ -z ${hood__dns} ] && echo -e '\n\nset ${hood__dns}\n\n^C' && sleep 1000

    git config --global user.name "baton@`hostname -s`"
    git config --global user.email "ops@${hood__dns}"

#### Link in ssh config

    # login: podium -> baton

    [ -z ${hood__name} ] && echo -e '\n\nset ${hood__name}\n\n^C' && sleep 1000

    cd ${HOME}/.ssh
    ln -s ${HOME}/work/hood/${hood__name}/things/share/ssh/config

#### Link in hood

    # login: podium -> baton

    [ -z ${hood__name} ] && echo -e '\n\nset ${hood__name}\n\n^C' && sleep 1000

    mkdir -p ${HOME}/.hood
    cd ${HOME}/.hood
    ln -s ${HOME}/work/hood/${hood__name}/things/conductor/baton/hood/vars


#### Set password for `root` user

    # login: podium -> baton -> root

    passwd

## SNAPSHOT the machine

    /--------------------------------------------------\
    |                                                  |
    |                                                  |
    |     S N A P S H O T   T H E   M A C H I N E      |
    |                                                  |
    |                                                  |
    |     REALLY!  This one was a lot of work to       |
    |     set up and is really important in the        |
    |     overall scheme of things.                    |
    |                                                  |
    |                                                  |
    \--------------------------------------------------/

## CONDUCT WITH INTEGRITY

      .,               _ '    `_      _______                               ( )
    --+-[---------.---(-)-----(@)----|-------|--.-----|-------------.-------|~--
      | ]         |   |~      |~ (@) _          |     |          |} |       |
    --+-[-----|---+---|-------|--|--(@)---------+-----|----------|}-+---|---|---
      |/      |   |   |       |  |  |~  (@)  _  | |  _| ..       |  |   |   |
    --Y-------|---+---|-------|--|--|---|---(@)-+-|>( )------|---|--+---|-------
     /|_     _|   |           `=_|  |   |   |~  |    ~       |>(@)  |  _|
    |-@-)---(@)---+-----------------|---|---|---+-------------------+-(@)-------
    \_|/     ~    |                     |   |   |                   |  ~
    --+-----------"-------------------------|---"-------------------"-----------
      |
    ._}

